package com.kevin;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.yaml.snakeyaml.Yaml;

import com.kevin.entiry.DbConfig;
import com.kevin.entiry.PosInfo;
import com.kevin.util.ExcelHandler;

class PosFactory {

//	private final static String configPath = PosFactory.class.getClassLoader().getResource("config.properties")
//			.getFile();

	public static void main(String[] args) throws Exception {

	}

	// load initial setting
	public void loadingConfig(String configPath, String dbType) {
		Yaml yaml = new Yaml();
		InputStream is = this.getClass().getClassLoader().getResourceAsStream("config.yaml");
		Map<String, Map<String, String>> m = yaml.load(is);
		Map<String, String> m2 = m.get(dbType);
		DbConfig db = new DbConfig(m2.get("url"), m2.get("user"), m2.get("pwd"));
		System.out.println(db);
	}
}