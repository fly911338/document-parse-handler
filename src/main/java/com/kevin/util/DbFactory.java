package com.kevin.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import com.kevin.entiry.DbConfig;

// Singleton
public class DbFactory {

	public DbConfig dbConfig;

	private static Connection conn = null;

	private static DbFactory dbFactory = null;

	private static final List<String> OPERATOR = Arrays.asList(">=", ">", "=", "!=", "<", "<=");

	public static void main(String[] args) {
		// 判斷是否為變數數字
//		a.matches("\\?.*\\?")  String a = "?abcd?";
		// 判斷是否為字串變數
//		b.matches("('|\")\\?.*\\?(\"|')")  String b = "'?handson?'";

//		String s = " select 1 from dual where '?apple?' = 'happy' ";
//		String s2 = " select 1 from dual where ?number? < 3  and 'superman' != '?var?' ";
		String s = "select 1 from dual where ?number? < 3  and 'superman' != '?var?' and '?var?' = 'happy' ";

		String[] sqlArray = s.trim().split("\\s+");

		System.out.println("start...");
		varHelper(sqlArray);
		for (String i : sqlArray) {
			System.out.print(i + " ");
		}
		System.out.println("end...");

		

	}

	/*
	 * TODO 1.現在處理的方式是用運算子為起點 但這樣沒辦法處理相同變數 應該要改成變數為起點 找出有幾個變數再抓運算子
	 * 這樣才不會有相同變數卻是不同值的錯誤狀況 2. 還沒處理日期狀況（去excel找出所有日期pattern加上去就可以了 應該滿簡單的 就麻煩） 3. is
	 * not 還有一些運算子沒有處理 4. 優化code
	 * 
	 */

	public static String[] varHelper(String[] sqlArray) {
		// 1. 找出變數
		LinkedList<Integer> varIndexList = new LinkedList<Integer>();
		for (int i = 0; i < sqlArray.length; i++) {
			if (sqlArray[i].matches("\\?.*\\?") || sqlArray[i].matches("('|\")\\?.*\\?(\"|')")) {
				varIndexList.add(i);
			}
		}
		// 2. 對變數數組跑迴圈 找出運算子 及對應值 （對應值特性 應位於運算子的前一個或後一個值）
		while (varIndexList.size() > 0) {
			int varIndex = varIndexList.pollFirst();
			Integer compareValueIndex = null;
			String opr = null;
			String compareValue = null;
			String varType = null;
			boolean iscompareValueIsVariable = false;
			Boolean isVarBeforeOperator = null;
			// 運算子預期應該是這變數的前(後)一個位置 如有非此狀況 目前會出錯 要等有相關case再進行調整
			// TODO 如果運算子有兩個這邊會不適用 例如 is not 因此現在不處理這種
			isVarBeforeOperator = OPERATOR.contains(sqlArray[varIndex + 1]) ? true : false;
			int offset = isVarBeforeOperator ? 1 : -1;
			opr = sqlArray[varIndex + offset];
			// index +-2 ?var? = 3
			compareValue = sqlArray[varIndex + offset * 2];
			// 3. 判斷變數及比對值資料型態
			// 判斷變數
			if (sqlArray[varIndex].matches("\\?.*\\?")) {
				varType = "String";
			} else if (sqlArray[varIndex].matches("('|\")\\?.*\\?(\"|')")) {
				varType = "number";
			}
			// 判斷比對值是否為變數
			if (sqlArray[varIndex].matches("\\?.*\\?") || sqlArray[varIndex].matches("('|\")\\?.*\\?(\"|')")) {
				iscompareValueIsVariable = true;
			}

			int defaultInteger = 1;
			String defaultString = "default";
			// 4. 依照運算子的規則及資料型態賦值給變數
			if (opr.equals("=") || opr.equals(">=") || opr.equals("<=")) {
				if (iscompareValueIsVariable) {
					sqlArray[varIndex] = varType.equals("String") ? defaultString : String.valueOf(defaultInteger);
					sqlArray[varIndex + offset * 2] = varType.equals("String") ? defaultString : String.valueOf(defaultInteger);
				}
				sqlArray[varIndex] = compareValue;
			} else if (opr.equals(">")) {
				sqlArray[varIndex] = iscompareValueIsVariable ?
						String.valueOf((defaultInteger + 1)) : String.valueOf(Integer.valueOf(compareValue) + 1);	
			} else if (opr.equals("<")) {
				sqlArray[varIndex] = iscompareValueIsVariable ?
						String.valueOf((defaultInteger - 1)) : String.valueOf(Integer.valueOf(compareValue) - 1);
			}

			// 5. 把已經使用的變數從陣列中取出
			if (iscompareValueIsVariable) {
				varIndexList.remove(compareValueIndex);
			}
		}

		return sqlArray;
	}

	private DbFactory(DbConfig db) throws SQLException {
		dbConfig = db;
		conn = DriverManager.getConnection(dbConfig.getUrl(), dbConfig.getUser(), dbConfig.getPwd());
	}

	public static Connection getConnection(DbConfig db) throws SQLException {
		if (dbFactory == null) {
			dbFactory = new DbFactory(db);
		}
		return conn;
	}

	public void close() throws SQLException {
		conn.close();
	}

}
