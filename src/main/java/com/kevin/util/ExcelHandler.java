package com.kevin.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.kevin.entiry.PosInfo;

public class ExcelHandler {

	/*
	 * 產報告流程 先有一組參數編號 代表要產報告得標號 利用其去excelReader把該欄資料抓出來
	 * 在執行generateReportTemplate產出報告
	 */

	public static void main(String[] args) throws Exception {
		// excel 14MB 通常18秒
//		Date start = new Date();
//		ExcelHandler a = new ExcelHandler();
//		List<PosInfo> list = new ArrayList<PosInfo>();
//		List<Integer> random_list = new ArrayList<Integer>();
//		for (int j = 0; j < 10; j++) {
//			int r = (int) (Math.random() * 100 + 1);
//			System.out.print(r + ",");
//			random_list.add(r);
//		}
//		System.out.println("\n===");
//		list = a.excelReader(random_list);
//
//		Date end = new Date();
//		list.stream().forEach(i -> System.out.println(i.getNo()));
//
//		System.out.println("耗時: " + (end.getTime() - start.getTime()) / 1000 + "秒");
		testExcelFunction();

	}

	public static String generateInsertSQL(String ruleName, String SQL, String alertMsg) {
		return "INSERT INTO LCMMODE VALUES ('" + ruleName + "', '000000', 'V', '" + SQL + "', '" + alertMsg + "')";
	}

	public static void testExcelFunction() throws Exception, IOException {
		XSSFWorkbook wb = new XSSFWorkbook();
		XSSFSheet sheet = wb.createSheet();
		XSSFRow row0 = sheet.createRow(0);
		XSSFCell cell00 = row0.createCell(0);
		cell00.setCellValue("SQL Template");
		XSSFCell cell10 = sheet.createRow(1).createCell(0);
		XSSFRow row4 = sheet.createRow(4);

//		XSSFCell cell40 = row4.createCell(0);
//		XSSFCell cell41 = row4.createCell(1);
//		XSSFCell cell42 = row4.createCell(2);
//		cell40.setCellValue("'?Token?'");
//		cell41.setCellValue("'?zzzz?'");
//		cell42.setCellValue("?apple?");
		// 這段模擬塞sql
		String formulaString = "select 1 from dual where ?apple? = and '?Token?' = 'HTC' " + "or '?zzzz?' > '?Token?' ";
		Set<String> set = new HashSet<String>();
		String[] excelVar = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L" };
		int index = 0;
		set.add("'?Token?'");
		set.add("'?zzzz?'");
		set.add("?apple?");
		for (String i : set) {
			if (i.matches("'\\?.*\\?'")) {
				formulaString = formulaString.replace(i, "'" + excelVar[index] + "4'");
			} else {
				formulaString = formulaString.replace(i, excelVar[index] + "4");
			}
			index++;
		}
		System.out.println(formulaString);
//		cell10.setCellFormula(formulaString);
		OutputStream os = new FileOutputStream(new File("/Users/liyanting/Downloads/excel/function.xlsx"));
		wb.write(os);
		os.close();
		wb.close();
	}

	// 讀取excel取出資訊
	public List<PosInfo> excelReader(List<Integer> noList) throws FileNotFoundException, IOException {
		List<PosInfo> list = new ArrayList<PosInfo>();
		FileInputStream fis = new FileInputStream("/Users/liyanting/Downloads/excel/a.xlsx");

		XSSFWorkbook wb = null;
		try {
			wb = new XSSFWorkbook(fis);
			XSSFSheet sheet = wb.getSheetAt(0);

			for (Row r : sheet) {
				Cell c = r.getCell(0);
				int n = (int) c.getNumericCellValue();
				if (noList.contains(n)) {
					list.add(getRowInfo(r, c.getRowIndex()));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (wb != null) {
				wb.close();
			}
		}

		return list;
	}

	public PosInfo getRowInfo(Row row, int rowIndex) {
		PosInfo pos = new PosInfo();
		pos.setNo(Integer.toString((int) row.getCell(0).getNumericCellValue()));
		pos.setSource(row.getCell(3).getStringCellValue());
		pos.setPendingType(row.getCell(4).getStringCellValue());
		pos.setAlertMessage(row.getCell(8).getStringCellValue());
		pos.setSystemType(row.getCell(14).getStringCellValue());
		pos.setReconfirm(row.getCell(15).getStringCellValue());
		// optional args
		try {
			row.getCell(13).getStringCellValue();
		} catch (NullPointerException e) {
			System.out.println("該列無照會類型");
		}
		return pos;
	}

	/*
	 * 產POS報告樣板
	 *
	 * reportDir: 產出報告目錄 會在其目錄下建一個日期目錄裡面放以pendingCode代碼為標題的報告
	 */
	public void generateReportTemplate(String reportDir, PosInfo pos) throws IOException {
		// 建制目錄 格式 reporDir / 本日日期 / 規則名稱.xlxs
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd");
		String date = sdf.format(new Date().getTime());
		File dir = new File(reportDir + date + "/");
		if (!dir.isDirectory()) {
			dir.mkdirs();
		}

		XSSFWorkbook wb = new XSSFWorkbook();
		Font font = wb.createFont();
		font.setFontName("微软雅黑");
		font.setFontHeightInPoints((short) 14);
		CellStyle style = wb.createCellStyle();
		style.setAlignment(HorizontalAlignment.CENTER_SELECTION);
		style.setVerticalAlignment(VerticalAlignment.CENTER);
		style.setFont(font);

		XSSFSheet sheet = wb.createSheet("報告");
		XSSFRow row0 = sheet.createRow(0);

		XSSFCell c0 = row0.createCell(0);
		c0.setCellStyle(style);
		c0.setCellValue("編號");
		XSSFCell c1 = row0.createCell(1);
		c1.setCellStyle(style);
		c1.setCellValue("pendingCode");

		sheet.addMergedRegion(new CellRangeAddress(0, 0, 2, 9));
		XSSFCell c2 = row0.createCell(2);
		c2.setCellStyle(style);
		c2.setCellValue("SQL");
		sheet.addMergedRegion(new CellRangeAddress(1, 4, 2, 9));
		XSSFRow row1 = sheet.createRow(1);
		XSSFCell c10 = row1.createCell(0);
		c10.setCellStyle(style);
		XSSFCell c11 = row1.createCell(1);
		c11.setCellStyle(style);
		XSSFCell c12 = row1.createCell(2);
		c12.setCellStyle(style);
		c10.setCellValue(pos.getNo());
		c11.setCellValue(pos.getPendingCode());
		c12.setCellValue(pos.getSql());

		// 刻成功失敗case
		CellRangeAddress region = CellRangeAddress.valueOf("A7:E18");
		CellRangeAddress region2 = CellRangeAddress.valueOf("G7:L18");
		sheet.addMergedRegion(region);
		sheet.addMergedRegion(region2);

		XSSFRow row6 = sheet.createRow(6);
		XSSFCell c60 = row6.createCell(0);
		c60.setCellStyle(style);
		c60.setCellValue("Success Case");
		XSSFCell c66 = row6.createCell(6);
		c66.setCellStyle(style);
		c66.setCellValue("Failure Case ");

		setBorderStyle(BorderStyle.MEDIUM, region, sheet);
		setBorderStyle(BorderStyle.MEDIUM, region2, sheet);

		// 調整寬度
		for (int i = 0; i < 12; i++) {
			sheet.autoSizeColumn(i);
			sheet.setColumnWidth(i, sheet.getColumnWidth(i) * 17 / 10);
		}

		try {
			FileOutputStream out = new FileOutputStream(dir + "/" + pos.getRuleName() + ".xlsx");
			wb.write(out);
			wb.close();
			out.flush();
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			System.out.println("finish");
		}
	}

	// 合併儲存格顯示邊界
	private void setBorderStyle(BorderStyle style, CellRangeAddress region, XSSFSheet sheet) {
		RegionUtil.setBorderLeft(style, region, sheet);
		RegionUtil.setBorderRight(style, region, sheet);
		RegionUtil.setBorderTop(style, region, sheet);
		RegionUtil.setBorderBottom(style, region, sheet);
	}

}
