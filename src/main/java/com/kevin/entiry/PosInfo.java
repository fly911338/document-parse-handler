package com.kevin.entiry;

public class PosInfo {
	
	// 編號
	private String no;
	
	private String sql;
	
	private String stauts;
	
	private String pendingCode;
	
	// 規則名稱
	private String ruleName;
	
	// 來源
	private String source;
	
	
	private String pendingType;
	
	// 照會文字
	private String alertMessage;
	
	// 照會類型
	private String alertType;
	
	// 系統類
	private String systemType;
	
	// 覆核
	private String reconfirm;

	public String getStauts() {
		return stauts;
	}

	public void setStauts(String stauts) {
		this.stauts = stauts;
	}

	public String getPendingCode() {
		return pendingCode;
	}

	public void setPendingCode(String pendingCode) {
		this.pendingCode = pendingCode;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getPendingType() {
		return pendingType;
	}

	public void setPendingType(String pendingType) {
		this.pendingType = pendingType;
	}

	public String getAlertMessage() {
		return alertMessage;
	}

	public void setAlertMessage(String alertMessage) {
		this.alertMessage = alertMessage;
	}

	public String getAlertType() {
		return alertType;
	}

	public void setAlertType(String alertType) {
		this.alertType = alertType;
	}

	public String getSystemType() {
		return systemType;
	}

	public void setSystemType(String systemType) {
		this.systemType = systemType;
	}

	public String getReconfirm() {
		return reconfirm;
	}

	public void setReconfirm(String reconfirm) {
		this.reconfirm = reconfirm;
	}

	public PosInfo(String source, String pendingType, String alertMessage, String alertType, String systemType,
			String reconfirm) {
		super();
		this.source = source;
		this.pendingType = pendingType;
		this.alertMessage = alertMessage;
		this.alertType = alertType;
		this.systemType = systemType;
		this.reconfirm = reconfirm;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public PosInfo() {
	}

	@Override
	public String toString() {
		return "PosInfo [sql=" + sql + ", \nno=" + no + ", stauts=" + stauts + ", pendingCode=" + pendingCode
				+ ", ruleName=" + ruleName + ", source=" + source + ", pendingType=" + pendingType 
				+ ",\nalertMessage=" + alertMessage + ", alertType=" + alertType + ", \nsystemType=" + systemType 
				+ ", reconfirm=" + reconfirm+ "]";
	}

	
	
	
	
	

}
