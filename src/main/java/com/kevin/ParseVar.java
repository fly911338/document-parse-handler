package com.kevin;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParseVar {

	public static void main(String[] args) {

		// case1 =CONCATENATE("select 1 from dual where ",C1," = and ",A1," = 'HTC' or
		// ",B1," > ",A1, )
		String sql = "select 1 from dual where ?apple? = and '?Token?' = 'HTC' " + "or '?zzzz?' > '?Token?' ";
		String result = handleInsertSQL(sql);
//		select 1 from dual where ?apple? = and '?Token?' = 'HTC' or '?zzzz?' > '?Token?' 
		System.out.println(sql);
//		select 1 from dual where ?apple? = and ''?Token?'' = 'HTC' or ''?zzzz?'' > ''?Token
		System.out.println(result);
		Map<String, String> map = handleSuccandFailCaseSQL(sql);
//		Success:=CONCATENATE("select 1 from dual where ",C20," = and ",A20," = 'HTC' or ",B20," > ",A20)
//		Failure:=CONCATENATE("select 1 from dual where ",I20," = and ",G20," = 'HTC' or ",H20," > ",G20)
		map.forEach((k,v)->System.out.println(k + ":" + v));

	}
	
	public static String handleInsertSQL(String sql) {
		Pattern p2 = Pattern.compile("('\\?\\w+\\?')");
		Matcher m2 = p2.matcher(sql);
		String insertSQL = sql;
		Set<String> set2 = new HashSet<String>();
		while (m2.find()) set2.add(m2.group(1));
		for(String k:set2) {
			insertSQL = insertSQL.replace(k, "'" + k + "'");
		}
		return insertSQL;
	}
	
	public static Map<String, String> handleSuccandFailCaseSQL(String sql) {
		Map<String, String> map = new HashMap<String, String>();
		String[] filterVarSQL = sql.trim().split("('?\\?\\w+\\?'?)");
		String[] SuccessExcelIndex = { "A20", "B20", "C20", "D20", "E20", "A22", "B22", "C22", "D22", "E22" };
		String[] FailureExcelIndex = { "G20", "H20", "I20", "J20", "K20", "G22", "H22", "I22", "J22", "K22" };
		int index = 0;
		Pattern p = Pattern.compile("('?\\?\\w+\\?'?)");
		Matcher m = p.matcher(sql);
		Set<String> set = new HashSet<String>();
		while (m.find()) {
			set.add(m.group(1));
		}
		for (String i : filterVarSQL) {
			sql = sql.replace(i, "\"" + i + "\"");
		}
		String SuccessSQL = sql;
		String FailureSQL = sql;
		// 增加｜是為了等等用|來切開個字串
		for (String j : set) {
			SuccessSQL = SuccessSQL.replace(j, "|" + SuccessExcelIndex[index] + "|");
			FailureSQL = FailureSQL.replace(j, "|" + FailureExcelIndex[index] + "|");
			index++;
		}
		String[] zz1 = SuccessSQL.trim().split("\\|");
		String[] zz2 = FailureSQL.trim().split("\\|");
		String succExcelSQL = "=CONCATENATE(";
		String failureExcelSQL = "=CONCATENATE(";
		for (int i = 0; i < zz1.length; i++) {
			succExcelSQL += i == zz1.length - 1 ? zz1[i] : zz1[i] + ",";
			failureExcelSQL += i == zz2.length - 1 ? zz2[i] : zz2[i] + ",";
		}
		succExcelSQL += ")";
		failureExcelSQL += ")";
		map.put("Success", succExcelSQL);
		map.put("Failure", failureExcelSQL);
		return map;
	}
}
