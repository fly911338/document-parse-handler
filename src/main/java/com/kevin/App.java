package com.kevin;

import java.io.InputStream;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.yaml.snakeyaml.Yaml;

import com.kevin.entiry.DbConfig;

public class App {
	private static DbConfig db = null;

	public static void main(String[] args) throws SQLException {
		String s = "select 1 from dual where ?apple? = and '?Token?' = 'HTC' "
				+ "or '?zzzz?' > '?Token?' ";
		String ss ="select 1 from dual where C = and A = 'HTC' or B > A";
		String[] excelVar = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"};
		Set<String> varList = new HashSet<String>();
		String[] z = s.split("('?\\?\\w+\\?'?)");
		for(String i:z) {
			System.out.println(i);
		}
////		// capture ?.*? and '?.*?'
//		Pattern p = Pattern.compile("('?\\?\\w+\\?'?)");
////		Map<String>
//		Matcher m = p.matcher(s);
//		while (m.find()) {
////			System.out.println(m.group(1));			
//			varList.add(m.group(1));
//		}
//		
//		System.out.println(s);
//		int index = 0;
//		for(String i:varList)  {
////			System.out.println(i);
//			s = s.replace(i, excelVar[index]);
//			index++;
//		}
//		System.out.println(s);
		
				
		
	}
	
	public static String s(String s) {
		return "";
	}
// a'a'
	public static String stringToXML(String s) {
		// 空字串處理
		if (s.trim().equals("")) return "";
		// 只擷取select 和 from之間的字串
		Pattern extractXMLInfo = Pattern.compile(".*select(.*)from.*");
		// 過濾單引號
		Pattern filterQuotation = Pattern.compile("'(.*)'");
		Matcher m = extractXMLInfo.matcher(s);
		m.find();
		String target = m.group(1).trim();
		// 利用逗號做分隔 但是單引號內的逗號不處理 ex ..., 'apple, banana' fruit, ...
		String[] splitByComma = target.split(",(?=(?:[^']*'[^']*')*[^']*$)", -1);
		StringBuilder XMLbuilder = new StringBuilder("<record><row>");
		for (String i : splitByComma) {
			String[] splitValueAndColumn = i.trim().split("\\s+");
			String value = splitValueAndColumn[0];
			String column = splitValueAndColumn[1];
			Matcher m2 = filterQuotation.matcher(value);
			value = m2.find() ? m2.group(1) : splitValueAndColumn[0];
			if (value.trim().equals("")) {
				XMLbuilder.append("<" + column + "/>");
			} else {
				XMLbuilder.append("<" + column + ">" + value + "</" + column + ">");
			}
		}
		XMLbuilder.append("</row></record>");
		return XMLbuilder.toString();
	}

	// load initial setting
	public void loadingConfig(String dbType) {
		Yaml yaml = new Yaml();
		InputStream is = this.getClass().getClassLoader().getResourceAsStream("config.yaml");
		Map<String, Map<String, String>> m = yaml.load(is);
		Map<String, String> m2 = m.get(dbType);
		db = new DbConfig(m2.get("url"), m2.get("user"), m2.get("pwd"));
		System.out.println(db);
	}

}
