package com.kevin.util;

import static org.junit.Assert.assertArrayEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class DbFactoryTest {

	@Test
	public void varHelperTestString() {
		String s3 = "select 1 from dual where ?number? < 3  and 'superman' != '?var?' " + "and '?var2?' = 'happy'";
		String[] sqlArray = s3.split("\\s+");
		List<String> operator = Arrays.asList(">=", ">", "=", "!=", "<", "<=");
		String[] res = DbFactory.varHelper(sqlArray);
		String[] expect = { "select", "1", "from", "dual", "where", "2", "<", "3", "and", "'superman'", "!=",
				"'supermannot'", "and", "'happy'", "=", "'happy'" };
		assertArrayEquals(res, expect);

	}

}
